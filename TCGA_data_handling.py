#!/usr/bin/python

from sys import argv
import glob,os

text_input = argv[1]
file = open(text_input, 'r')

s1=set([])

for line in file.readlines():
    if (line[0] != '#'):
        mylist = line.rstrip().split()
        s1=s1.union(set([mylist[0]]))
# print s1

dir_input = argv[2]
file_dir_extension = os.path.join(dir_input, '*.genes.results')

s2=set([])

for file_name in glob.glob(file_dir_extension):
    if file_name.endswith('.genes.results'):
        # print file_name
        import re
        file_name_split = re.split('[.]', file_name)
        file_name_split_s = file_name_split[2]
        # print type(file_name_split_s)

        s2=s2.union([file_name_split[2]])
        # print file_name_split[2]

# print s2

differences = []

for list in s1:
    if list not in s2:
        differences.append(list)
print differences