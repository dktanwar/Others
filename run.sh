#!/bin/sh

script=/home/deepak/TCGA_Project/bin/32_2_gene_cor_pval.R 
for i in `find /home/deepak/TCGA_Project/results/16_OV_all_genes_split -name "*.txt"`;do
	for j in `find /home/deepak/TCGA_Project/results/16_OV_all_genes_split -name "*.txt"`; do  
	file1=`basename $i .txt`
	file2=`basename $j .txt`
	outfile=${file1}-${file2}
	qsub -b y -j y -e ${outfile}.log -cwd -V "Rscript ${script} $i $j >${outfile}.txt && bzip2 ${outfile}.txt"
	done
done
